from flask import Flask, render_template, request, redirect, url_for, flash
from random import shuffle

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login', methods=['POST'])
def login():
    email = request.form.get('email')
    if email:
        # Write email to a text file
        try:
            with open('emails.txt', 'a') as f:
                f.write(email + '\n')
            print("Email written to file successfully.")
        except Exception as e:
            print("Error writing email to file:", e)
        return redirect(url_for('welcome', email=email))
    else:
        flash('Invalid email address')
        return redirect(url_for('index'))

@app.route('/welcome')
def welcome():
    email = request.args.get('email')
    return render_template('welcome.html', email=email)

if __name__ == '__main__':
    app.run(debug=True)